In lust's embrace I'm drawn to you  
Desire's flame burns deep and true  
Your allure, a tempting spell  
In your presence, I long to dwell  
